#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<graphics.h>
#include<string.h>
#include<conio.h>
#include<time.h>

IMAGE img[12];//保存12个图片
int imgIndex[12] = { 0,2,4,8,16,32,64,128,256,512,1024,2048 };
int map[4][4] = { 0 };

/*
* 加载资源
*/
void loadResourcy()
{
	for (int i = 0; i < 12; i++)
	{
		char filename[20] = "";
		sprintf(filename, "%d.png", imgIndex[i]);
		loadimage(img + i, filename,60,60);
	}
}
/*
* 画地图
*/
void drawMap()
{
	setbkcolor(RGB(244, 215, 215));
	cleardevice();
	settextcolor(WHITE);
	settextstyle(35, 0, "楷体");
	outtextxy(50, 10, "2048游戏");
	//根据二维数组地租的值画图
	int x, y, k;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			x = 60 * j;
			y = 60 * i + 60;
			//判断是哪张图
			for (k = 0; k < 12; k++)
			{
				if (imgIndex[k] == map[i][j])
					break;
			}
			putimage(x, y, img + k);
		}
	}
}
int randIntNum()
{
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (map[i][j] == 0)
			{
				map[i][j] = (rand() %3)*2;
				if (map[i][j] == 0)
					continue;
				return 0;
			}
		}
	}
	return 0;
}

void keyDown()
{
	char key = _getch();
	switch (key)
	{
	case 'W':
	case 'w':
	case 72:
		randIntNum();
		break;
	case 'S':
	case 's':
	case 80:
		randIntNum();
		break;
	case 'A':
	case 'a':
	case 75:
		randIntNum();
		break;
	case 'D':
	case 'd':
	case 77:
		randIntNum();
		break;
	}
}

int main0()
{
	initgraph(60*4, 60*4+60,1);
	loadResourcy();
	drawMap();
	while(1)
	{
		keyDown();
		drawMap();
	}
	getchar();//防止闪屏
	closegraph();
	//system("cls");
	return 0;
}