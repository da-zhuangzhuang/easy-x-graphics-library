#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int removeElement(int* nums, int numsSize, int val) {
    int* fast = nums;
    int* slow = nums;
    while ((*slow) != val)
    {
        slow++;
    }
    fast = slow;
    while (fast < (nums + numsSize))
    {
        // while(*slow!=val)
        // {
        //     slow++;
        // } 
        while (fast < (nums + numsSize) && (*fast) == val)
        {
            fast++;
        }
        if (fast < (nums + numsSize))
        {
            (*slow) = (*fast);
            slow++;
            fast++;
        }
    }
    int count = 0;
    while (slow != nums)
    {
        count++;
        slow--;
    }
    return count-1;
}
int main()
{

    int nums [] = {0, 1, 2, 2, 3, 0, 4, 2};
    int val = 2;

    removeElement(nums, 8, 2);
    for (int i = 0; i < removeElement(nums, 8, 2); i++)
    {
        printf("%d ", nums[i]);
    }
    printf("\n");

	return 0;
}