#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<easyx.h>
#include<stdbool.h>
#include <mmsystem.h>       
#include<Windows.h>//多媒体库
#pragma comment(lib,"winmm.lib")    //静态库资源
IMAGE pf;
#define COL 15//列
#define ROW 15//行
#define GRID_W 25
#define SPACE (2*GRID_W)
enum Chess {
	None,
	Black,
	White
};
struct NowPos {
	int row;
	int col;
	bool isShow;
	int player;
}pos = { -5,-5,false,Black };
//定义数组保存棋子
int Chess[ROW][COL] = { 0 };
void loadpicture()
{
	loadimage(&pf,"background.png", 450, 450);
}
void drawline()
{
	putimage(0, 0, &pf);
	setlinecolor(BLACK);
	for (int i = 0; i <COL; i++)
	{
		setlinestyle(PS_SOLID, 1);
		line(0 + SPACE, i * GRID_W + SPACE, (ROW-1)*GRID_W + SPACE, i * GRID_W + SPACE);
	}
	for (int i = 0; i < ROW; i++)
	{
		setlinestyle(PS_SOLID, 1);
		line(i * GRID_W + SPACE, 0 + SPACE, i * GRID_W + SPACE, (ROW - 1) * GRID_W+SPACE);
	}
	setlinestyle(PS_SOLID, 2);
	rectangle(0 + SPACE, 0 + SPACE, 0 + SPACE + (ROW - 1) * GRID_W, 0 + SPACE + (ROW - 1) * GRID_W);
	setfillcolor(BLACK);
	solidcircle(0 + SPACE +7* GRID_W, 0 + SPACE + 7 * GRID_W, 5);
	solidcircle(0 + SPACE + 3 * GRID_W, 0 + SPACE + 3 * GRID_W, 5);
	solidcircle(0 + SPACE + 3 * GRID_W, 0 + SPACE + 11 * GRID_W, 5);
	solidcircle(0 + SPACE + 11 * GRID_W, 0 + SPACE + 3 * GRID_W, 5);
	solidcircle(0 + SPACE + 11 * GRID_W, 0 + SPACE + 11 * GRID_W, 5);
	//根据数组里面数据绘制棋子
	for (int i = 0; i < ROW; i++)
	{
		for (int k = 0; k < COL; k++)
		{
			if (Chess[i][k] == Black)
			{
				setfillcolor(BLACK);
				solidcircle(i * GRID_W + SPACE, k * GRID_W + SPACE, 10);
			}
			if (Chess[i][k] == White)
			{
				setfillcolor(WHITE);
				solidcircle(i * GRID_W + SPACE, k * GRID_W + SPACE, 10);
			}
		}
	}
	//绘制框框
	/*if (pos.col*GRID_W >= SPACE && pos.col * GRID_W <= SPACE + 14 * GRID_W && pos.row * GRID_W >= SPACE && pos.row * GRID_W <= 14 * GRID_W + SPACE)
	{
		pos.isShow = true;
	}
	else
	{
		pos.isShow = false;
	}*/
	if (pos.isShow)//在范围内就画
	{
		setlinecolor(RED);
		/*rectangle(pos.col*GRID_W-8 , pos.row*GRID_W-8 , pos.col * GRID_W + 8, pos.row * GRID_W + 8);*/
		//rectangle(pos.col - 10, pos.row - 10, pos.col + 10, pos.row + 10);
		rectangle(SPACE+pos.col * GRID_W - 8, SPACE + pos.row * GRID_W - 8, SPACE + pos.col * GRID_W + 8, SPACE + pos.row * GRID_W + 8);
	}

}
//鼠标移动
void mouseMoveMsg(ExMessage* msg)
{
	pos.isShow = false;
	/*pos.col=msg->x/GRID_W;
	pos.row=msg->y/GRID_W;*/
	//pos.col = msg->x;
	//pos.row = msg->y;
	//坐标校准
	for (int i = 0; i < ROW; i++)
	{
		for (int k = 0; k < COL; k++)
		{
			int gridex = k * GRID_W + SPACE;
			int gridey = i * GRID_W + SPACE;
			if (abs(msg->x - gridex) <= 12 && abs(msg->y - gridey) <= 12)
			{
				pos.isShow = true;
				pos.row = i;
				pos.col = k;
			}
		}
	}
}
bool check_five(int i, int j)
{
	if (Chess[i][j] == Chess[i - 1][j] && Chess[i][j] == Chess[i - 2][j] && Chess[i][j] == Chess[i + 1][j] && Chess[i][j] == Chess[i + 2][j])//横行判断
		return true;
	if (Chess[i][j] == Chess[i][j - 1] && Chess[i][j] == Chess[i][j - 2] && Chess[i][j] == Chess[i][j + 1] && Chess[i][j] == Chess[i][j + 2])//纵行判断
		return true;
	if (Chess[i][j] == Chess[i - 1][j - 1] && Chess[i][j] == Chess[i - 2][j - 2] && Chess[i][j] == Chess[i + 1][j + 1] && Chess[i][j] == Chess[i + 2][j + 2])
		return true;
	if (Chess[i][j] == Chess[i - 1][j + 1] && Chess[i][j] == Chess[i + 2][j - 2] && Chess[i][j] == Chess[i + 1][j - 1] && Chess[i][j] == Chess[i - 2][j + 2])
		return true;
	return false;
}

int winner = 0;
bool check_over()
{
	for (int i = 0; i < 15; i++)
	{
		for (int j = 0; j < 15; j++)
		{
			if (Chess[i][j] == 0)
				continue;
			if (check_five(i, j))
			{
				winner = Chess[i][j];
				return true;
			}
		}
	}
	return false;
}



bool full()
{
	for (int i = 0; i < ROW; i++)
	{
		for (int k = 0; k < COL; k++)
		{
			if (Chess[i][k] == 0)
				return false;
		}
	}
	return true;;
}
//鼠标按下
bool mousePressMsg(ExMessage* msg)
{
	if (msg->message == WM_LBUTTONDOWN)
	{
		if (pos.col>=0&&pos.col<15&& pos.row >= 0 && pos.row < 15 &&Chess[pos.col][pos.row] == 0)
		{
			Chess[pos.col][pos.row] = pos.player;
			//判断输赢
			if (check_over())
			{
				for (int i = 0; i < ROW; i++)
				{
					for (int k = 0; k < COL; k++)
					{
						if (Chess[i][k] == Black)
						{
							setfillcolor(BLACK);
							solidcircle(i * GRID_W + SPACE, k * GRID_W + SPACE, 10);
						}
						if (Chess[i][k] == White)
						{
							setfillcolor(WHITE);
							solidcircle(i * GRID_W + SPACE, k * GRID_W + SPACE, 10);
						}
					}
				}
				if (winner == Black)
				{
					MessageBox(GetHWnd(), "黑棋赢了", "over", MB_OK);
					return true;
				}
				else
				{
					MessageBox(GetHWnd(), "白棋赢了", "over", MB_OK);
					return true;
				}
			}
			if (full())
			{
				MessageBox(GetHWnd(), "平局", "over", MB_OK);
				return true;
			}

			//切换棋手
			pos.player = pos.player == Black ? White : Black;
		}
		return false;
	}
}
int main()
{
	mciSendString("open 1.mp3", 0, 0, 0);
	mciSendString("play 1.mp3", 0, 0, 0);
	initgraph(450, 450);
	loadpicture();
	//draw();
	bool isover = true;
	while (isover)
	{
		BeginBatchDraw();
		drawline();
		EndBatchDraw();
		ExMessage msg;
		if (peekmessage(&msg,EX_MOUSE))//获取鼠标信息
		{
			switch (msg.message)
			{
			case WM_LBUTTONDOWN://鼠标左键按下
				if (mousePressMsg(&msg))
					isover = false;
				break;
			case WM_MOUSEMOVE://鼠标移动
				mouseMoveMsg(&msg);
				break;
			default:
				break;
			}
		}
	}
	closegraph();

	return 0;
}