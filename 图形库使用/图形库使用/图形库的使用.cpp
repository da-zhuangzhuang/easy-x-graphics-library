#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<windows.h>
#include<time.h>
#include <graphics.h>//这个头文件，包含了已经被淘汰的函数，都有
//#include<easyx.h>//这个只包含最新的函数
#include<stdbool.h>
#include<mmsystem.h>//多媒体库
#pragma comment(lib,"winmm.lib")//静态库资源


//两个函数就可以创建窗口
// initgraph(int x,int y,int style ->显示样式);显示窗口
// closegraph();关闭窗口



//设置窗口属性
/*
* 颜色设置
* ：setbkcolor
*   ：：颜色宏（颜色的英文大写）
* or ：：RGB配置:
* 
* 刷新
* 设置完后要刷新：cleardevice();
*/


//基本绘图函数
/*
*  画线 line(x,y,xx,yy); 参数  起始x,y到终点xx,yy
*  画圆 circle(x,y,r);   参数  圆心坐标 x,y，半径r
* 画矩形 rectangle（x,y,xx,yy);
*/
 //解决闪烁，解决方案：批量绘图


//图形化窗口是不支持C语言的输入输出函数的


//封装一个矩形
//typedef struct rectangle {
//	int x;
//	int y;
//	int w;
//	int h;
//}Rect;
//
////判断鼠标是否在按钮(矩形范围)上
//bool contains(ExMessage* msg, Rect rect)
//{
//	//鼠标位置坐标
//	int mx = msg->x;
//	int my = msg->x;
//	//矩形范围坐标
//	int rx = rect.x;
//	int ry = rect.y;
//	int rxx = rect.x + rect.w;
//	int ryy = rect.y + rect.h;
//	return mx >= rx && mx <= rxx && my >= ry && my <= ryy;
//}
//
////按钮绘制
//bool button_text(int x, int y, const char* text, ExMessage* msg)
//{
//	//绘制
//	/*
//	* 1 绘制矩形
//	* 2 绘制文字
//	*/
//	settextstyle(60,0,"微软雅黑");
//	//获取文字宽度和高度
//	int text_w = textwidth(text);
//	int text_h = textheight(text);
//
//	//按钮宽度和高度
//	Rect rect = { x,y,text_w + 60,text_h + 10 };
//	COLORREF color = WHITE;
//	setlinestyle(PS_SOLID, 2);
//	bool isHover= contains(msg, rect);
//	setfillcolor(isHover == true ? LIGHTBLUE : WHITE);
//	setlinecolor(BLACK);
//	fillrectangle(rect.x, rect.y, rect.x + rect.w, rect.y + rect.h);
//	//消息处理
//	return false;
//}
//
//
//int x;
//int y;
//int main()
//{
//	srand((unsigned int)time(NULL));
//
//	initgraph(800, 600, 1);//这里的1就是显示控制台
//	setbkcolor(BLACK);//RGB配置颜色设置
//	cleardevice();
//	mciSendString("open 1.mp3", 0, 0, 0);
//	mciSendString("play 1.mp3", 0, 0, 0);
//	//开始批量绘图
//	BeginBatchDraw();
//
//	while(1)
//	{
//		ExMessage msg;
//		getmessage(&msg);
//		
//		settextcolor(RGB(234, 54, 128));//设置字体颜色，对于在他后面输出的字体有效
//		settextstyle(20, 20, "黑体");//设置字体样式，对于在他后面输出的字体有效
//		outtextxy(60, 430, "WL我错了，我错了，请给我改正的机会！");
//		settextcolor(RGB(181, 184, 234));
//		outtextxy(30, 480, "我又梦到你好多次,给我一个机会！");
//		settextcolor(RGB(234, 55, 14));
//		
//
//		//定义图片
//		IMAGE img;
//		loadimage(&img, "2.jpg", 500, 300);//加载图片
//		putimage(0, 0, &img);//打印图片
//		loadimage(&img, "1.jpg", 250, 250);
//		putimage(520, 100, &img);
//		button_text(200, 500, "YES", &msg);
//		button_text(100, 500, "NO", &msg);
//		FlushBatchDraw();
//	}
//	//结束批量绘图
//	EndBatchDraw();
//	//关闭窗口
//	closegraph();
//	return 0;
//}