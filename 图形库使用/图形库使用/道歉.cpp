#define _CRT_SECURE_NO_WARNINGS 1
//#include <graphics.h>     
#include<easyx.h>
//图形库头文件,需要安装一下
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <thread>
#include <stdbool.h>
#include <mmsystem.h>       
#include<Windows.h>//多媒体库
#pragma comment(lib,"winmm.lib")    //静态库资源

//显示图像
void label_image(int x, int y, int w, int h, const char* imgURL)
{
    IMAGE img;
    loadimage(&img, imgURL, w, h);
    putimage(x, y, &img);
}
//显示文字
void label_text(int x, int y, const char* text)
{
    settextstyle(40, 0, "黑体");
    settextcolor(RGB(69,119,116));
    outtextxy(x, y, text);
}
//封装按钮
typedef struct Rect
{
    int x;
    int y;
    int w;
    int h;
}Rect;
//判断鼠标是否在按钮上
bool contains(ExMessage* msg, Rect rect)
{
    //鼠标的坐标
    int mx = msg->x;
    int my = msg->y;
    //矩形的左上角和右下角
    int x = rect.x;
    int y = rect.y;
    int xx = rect.x + rect.w;
    int yy = rect.y + rect.h;
    return mx >= x && mx <= xx && my >= y && my <= yy;
}

//按钮绘制
bool button_text(int x, int y, const char* text, ExMessage* msg)
{
    //绘制操作
    //1.绘制矩形
    //2.绘制文字
    settextstyle(60, 0, "微软雅黑");
    //获取文字的宽度和高度
    int text_w = textwidth(text);
    int text_h = textheight(text);
    //获取到按钮的宽度和高度
    Rect rect = { x,y,text_w + 60,text_h + 10 };
    COLORREF color = WHITE;
    //绘制矩形
    setlinestyle(PS_SOLID, 2);      //设置线的样式
    bool isHover = contains(msg, rect);
    setfillcolor(isHover == true ? LIGHTBLUE : WHITE);
    setlinecolor(BLACK);
    fillrectangle(rect.x, rect.y, rect.x + rect.w, rect.y + rect.h);
    //绘制文字
    settextcolor(RED);
    //去掉文字背景
    setbkmode(TRANSPARENT);
    outtextxy(rect.x + 30, rect.y + 5, text);
    //消息处理
    if (isHover == true && msg->message == WM_LBUTTONDOWN)
    {
        return true;
    }
    if (isHover == true && msg->message == WM_LBUTTONUP)
    {
        return false;
    }
    return false;
}

int  main()
{
    srand((unsigned int)time(NULL));
    initgraph(800, 600);
    setbkcolor(BLACK);
    //setbkcolor(RGB(147, 254, 1));
    cleardevice();
    mciSendString("open 1.mp3", 0, 0, 0);
    mciSendString("play 1.mp3", 0, 0, 0);
    //准备一个变量存储点NO生成的YES按钮
    Rect dst[99];
    int  count = 0; //记录生成的按钮个数
    bool isRunning = true;
    BeginBatchDraw();
    while (isRunning)
    {
        ExMessage msg;
        getmessage(&msg);
        label_text(10, 380, "WL,我错了我错了!");
        label_text(30, 430, "原谅我这一次！！！");
        label_image(0, 0, 500, 300, "2.jpg");
        label_image(550, 50, 250, 250, "1.jpg");
        label_image(400, 300, 350, 300, "3.jpg");
        if (button_text(10, 500, "YES", &msg))
        {
            //要加音效的可以自己尝试着加一下

            isRunning = false;
        }
        if (button_text(200, 500, "NO", &msg))
        {
           
            //画心形状的YES按钮
            static double t = 0;
            static double factor = 15;
            if (t < 6.4)
            {
                t += 0.3;
            }
            else
            {
                t = 0;
                factor -= 10;
            }
            dst[count].x = factor * 16 * pow(sin(t), 3);
            dst[count].y = factor * (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t));
            count++;
        }
        for (int i = 0; i < count; i++)
        {
            if (button_text(dst[i].x + 300, -dst[i].y + 250, "YES", &msg))
            {
                isRunning = false;
            }
        }
        Sleep(20);
        FlushBatchDraw();
    }
    EndBatchDraw();
    closegraph();
    return 0;
}