#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
char* replaceSpace(char* s) {
    int len = strlen(s);
    int old_end = len + 1;
    int count = 0;
    for (int i = 0; i < len; i++) {
        if (s[i] == ' ') {
            count++;
        }
    }
    int new_end = old_end + (count * 2);
    char* newarr = (char*)malloc(new_end * sizeof(char));
    while (old_end >= 0 && new_end >= 0) {
        if (s[old_end] != ' ') {
            newarr[new_end] = s[old_end];
            old_end--;
            new_end--;
        }
        else if (s[old_end] == ' ') {
            newarr[new_end--] = '0';
            newarr[new_end--] = '2';
            newarr[new_end--] = '%';
            old_end--;
        }
    }

    return newarr;
}

int main()
{
    char arr[] = "We Are Happy";
    int len = strlen(arr);
   // replaceSpace(arr);
    printf("%s\n", replaceSpace(arr));
    return 0;
}