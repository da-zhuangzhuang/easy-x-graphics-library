#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>



/*
第一行包含两个整数n和m，表示一个矩阵包含n行m列，用空格分隔。 (1≤n≤10,1≤m≤10)
从2到n+1行，每行输入m个整数（范围-231~231-1），用空格分隔，共输入n*m个数，表示第一个矩阵中的
元素。
接下来一行输入k，表示要执行k次操作（1≤k≤5）。接下来有k行，每行包括一个字符t和两个数a和b，中间
用空格格分隔，t代表需要执行的操作，当t为字符'r'时代表进行行变换，当t为字符'c'时代表进行列变换，
a和b为需要互换的行或列（1≤a≤b≤n≤10，1≤a≤b≤m≤10）
*/

int main0(){
    int n = 0, m = 0;
    scanf("%d%d", &n, &m);
    int arr[10][10] = { 0 };
    int i = 0, j = 0;
    for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) {
            scanf("%d", &arr[i][j]);
        }
    }
    int k = 0;
    scanf("%d", &k);
    char t = ' ';
    int a = 0, b = 0;
    for (i = 0; i < k; i++) {
        scanf(" %c %d %d", &t, &a, &b);
        if (t == 'r') {
            for (j = 0; j < m; j++) {
                int tmp = arr[a - 1][j];
                arr[a - 1][j] = arr[b - 1][j];
                arr[b - 1][j] = tmp;
            }
        }
        else if (t == 'c') {
            for (j = 0; j < n; j++) {
                int tmp = arr[j][a - 1];
                arr[j][a - 1] = arr[j][b - 1];

                arr[j][b - 1] = tmp;
            }
        }
    }
    for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}