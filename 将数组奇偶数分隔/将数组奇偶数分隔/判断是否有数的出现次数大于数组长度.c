﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
/*
* 给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
*/

int com(void* e1, void* e2)
{
    return *((int*)e1) - *((int*)e2);
}
int majorityElement(int* nums, int numsSize) {
    if (nums == NULL)
    {
        return 0;
    }
    qsort(nums, numsSize, sizeof(int), com);
    // int i=0;
    // int count=0;
    // for(i=0;i<numsSize-1;i++)
    // {
    //     if(nums[i]==nums[i+1])
    //     {
    //         count=i+2;
    //     }
    //     else{
    //         if(count>(numsSize/2))
    //         {
    //             return nums[i];
    //         }
    //         count=0;
    //     }
    // }

    int sz = numsSize / 2;
    int target = nums[sz];
    int count = 0;
    for (int i = 0; i < numsSize; i++)
    {
        if (nums[i] == target)
            count++;
    }
    if (count > sz)
        return target;
    else
        return 0;


}

int main1()
{
    int arr[] = { 1,2,2,2,3,3,3,2,2,2,2,5,10,5,6 };
    int len = sizeof(arr) / sizeof(arr[0]);
    qsort(arr,len, sizeof(int), com);
    if (majorityElement(arr, len))
    {
        printf("%d\n", majorityElement(arr, len));
    }
    else
    {
        printf("没有该数字！\n");
    }
	return 0;
}