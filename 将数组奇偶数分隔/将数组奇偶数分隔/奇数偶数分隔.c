#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void exchange1(int* arr, int len)
{
	int left = 0;
	int right = len - 1;
	while (left < right)
	{
		while ((left < right) && ((arr[left] & 1) == 1))
		{
			left++;
		}
		while ((left < right) && ((arr[right] & 1) == 0))
		{
			right--;
		}
		if (left < right)
		{
			int temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
		}
	}
}

//调整奇偶后之前数字的相对位置不能变
void exchange2(int* arr, int len)
{
	int k = 0;
	for (int i = 0; i < len; i++)
	{
		if ((arr[i] % 2) != 0)
		{
			int temp = arr[i];
			int j = i;
			while (j > k)
			{
				arr[j] = arr[j - 1];
				j--;
			}
			arr[k++] = temp;
		}
	}
}
int main0()
{
	int arr[] = { 1,10,3,4,100,101,5,6,7,50,11,9,2 ,13,16,57};
	int len = sizeof(arr) / sizeof(arr[0]);
	//exchange1(arr, len);
	exchange2(arr, len);



	/*int left = 0;
	int right = len - 1;
	while (left < right)
	{
		while ((left < right) && ((arr[left] & 1) == 1))
		{
			left++;
		}
		while ((left < right) && ((arr[right] & 1) == 0))
		{
			right--;
		}
		if(left<right)
		{
			int temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
		}
	}*/
	for (int i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}
