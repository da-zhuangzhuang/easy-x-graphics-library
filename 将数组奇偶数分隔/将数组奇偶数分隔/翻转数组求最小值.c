#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

/*
* 有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，
变成一个旋转数组，比如变成了[3,4,5,1,2]，或者[4,5,1,2,3]这样的。请问，给定这样一个旋转数组，求数组中的最小值。
*/

//将旋转数组分成两部分，将前部分放到后一部分的后面
//那么最小值就是新数组后部分的第一个值
int minNumberInRotateArray(int* rotateArray, int rotateArrayLen) {
    int left = 0, right = rotateArrayLen - 1;
    while (left < right)
    {
        int mid = (left + right) / 2;//取中间值
        if (rotateArray[mid] > rotateArray[right])
            left = mid + 1;//中间值大于最后一个值说明中间值不属于后部分，
        //而属于前部分，那么最小值就在后部分，搜索中间值的右边
        else if (rotateArray[mid] < rotateArray[right])
            right = mid;//中间值小于最后一个值说明中间值属于后部分，
        //那么最小值就在中间值及其前面的数中找
        else            //如果中间值等于最后一个值，那么不知道它是属于前部分还是后部分
            right--;    //缩短数组
    }
    return rotateArray[left];
}

int main22()
{
    int arr[] = { 5,1,2,3,5,5,5,5,5,5,5,5};
    int len = sizeof(arr) / sizeof(arr[0]);
    int tar = minNumberInRotateArray(arr, len);
    printf("最小值为: %d\n", tar);
	return 0;
}
