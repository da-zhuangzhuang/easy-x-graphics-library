#pragma once
//窗口宽
#define WINDOW_WIDTH        640
//窗口高
#define WINDOW_HEITH        478
//背景图top高
#define TOP_HEITH           192
//背景图mid_up高
#define MID_UP_HEITH        120
//背景图mid_down高
#define MID_DOWN_HEITH      167
//背景图bottom高
#define BOTTOM_HEITH        116
//背景图mid_up起始y坐标
#define MID_UP_BEGIN_Y      164
//背景图mid_down起始y坐标
#define MID_DOWN_BEGIN_Y    247
//背景图bottom起始y坐标
#define BOTTOM_BEGIN_Y      364
//小地鼠宽
#define LITTLE_MOUSE_WIDTH  90
//小地鼠高
#define LITTLE_MOUSE_HEITH  74
//中地鼠宽
#define MID_MOUSE_WIDTH     102
//中地鼠高
#define MID_MOUSE_HEITH     86
//大地鼠宽
#define BIG_MOUSE_WIDTH     122
//大地鼠高
#define BIG_MOUSE_HEITH     106


//小地鼠x起始位置
#define LITTLE_MOUSE_BEGIN_X    135
//小地鼠y起始位置
#define LITTLE_MOUSE_BEGIN_Y    191
//小地鼠间距
#define LITTLE_MOUSE_SPACE      137

//中地鼠x起始位置
#define MID_MOUSE_BEGIN_X       106
//中地鼠y起始位置
#define MID_MOUSE_BEGIN_Y       283
//中地鼠间距
#define MID_MOUSE_SPACE         156

//大地鼠x起始位置
#define BIG_MOUSE_BEGIN_X       65
//大地鼠y起始位置
#define BIG_MOUSE_BEGIN_Y       410
//大地鼠间距
#define BIG_MOUSE_SPACE         185