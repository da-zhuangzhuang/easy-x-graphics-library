#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<graphics.h>��
#include"ͼƬ����.h"
#include <time.h>
#include<windows.h>

#define TEST  1
#define SPACE  3
#define NUM 9

//������
struct DiShu {
	int x, y;
	IMAGE bmp, bmp_y;
	bool isUp;

	void show() {
		putimage(x, y, &bmp_y, SRCAND);
		putimage(x, y, &bmp, SRCPAINT);
	}
	void moveUp() {
		y -= SPACE;
	}
	void moveDown() {
		y += SPACE;
	}
};
struct DiShu ds[NUM];

IMAGE top, mid_up, mid_down, bottom;
IMAGE top_y, mid_up_y, mid_down_y, bottom_y;
IMAGE little_mouse, mid_mouse, big_mouse;
IMAGE little_mouse_y, mid_mouse_y, big_mouse_y;

int m;
//��ʼ��
void initGame();
//��ʾ
void showGame();
//����
void ctolGame();

int main() {
	initGame();
	while (1) {
		showGame();
		ctolGame();
      //  Sleep(100);
	}
    closegraph();
	return 0;
}



//��ʼ��
void initGame() {
    //0. ��ͼ�ν��洰��
    initgraph(WINDOW_WIDTH, WINDOW_HEITH);

    loadimage(&little_mouse, "little_mouse.bmp");
    loadimage(&mid_mouse, "mid_mouse.bmp");
    loadimage(&big_mouse, "big_mouse.bmp");

    loadimage(&little_mouse_y, "little_mouse_y.bmp");
    loadimage(&mid_mouse_y, "mid_mouse_y.bmp");
    loadimage(&big_mouse_y, "big_mouse_y.bmp");

    loadimage(&top, "top.bmp");
    loadimage(&mid_up, "mid_up.bmp");
    loadimage(&mid_down, "mid_down.bmp");
    loadimage(&bottom, "bottom.bmp");

    loadimage(&top_y, "top_y.bmp");
    loadimage(&mid_up_y, "mid_up_y.bmp");
    loadimage(&mid_down_y, "mid_down_y.bmp");
    loadimage(&bottom_y, "bottom_y.bmp");

    //��ֵ

    for (int i = 0; i < NUM; i++) {
        ds[i].isUp = true;
        if (i >= 0 && i < 3) {
            ds[i].bmp = little_mouse;
            ds[i].bmp_y = little_mouse_y;

            ds[i].x = LITTLE_MOUSE_BEGIN_X + i * LITTLE_MOUSE_SPACE;
            ds[i].y = LITTLE_MOUSE_BEGIN_Y;
        }
        if (i >= 3 && i < 6) {
            ds[i].bmp = mid_mouse;
            ds[i].bmp_y = mid_mouse_y;

            ds[i].x = MID_MOUSE_BEGIN_X + (i - 3) * MID_MOUSE_SPACE;
            ds[i].y = MID_MOUSE_BEGIN_Y;
        }
        if (i >= 6 && i < 9) {
            ds[i].bmp = big_mouse;
            ds[i].bmp_y = big_mouse_y;

            ds[i].x = BIG_MOUSE_BEGIN_X + (i - 6) * BIG_MOUSE_SPACE;
            ds[i].y = BIG_MOUSE_BEGIN_Y;
        }
    }

    srand(time(0));

    m = rand() % NUM;//���ѡһ��
}

//��ʾ
void showGame() {
    BeginBatchDraw();


    putimage(0, 0, &top_y, SRCAND);
    putimage(0, 0, &top, SRCPAINT);

    //С����
    for (int i = 0; i < 3; i++) {
        ds[i].show();
    }
#if TEST
    putimage(0, MID_UP_BEGIN_Y, &mid_up_y, SRCAND);
    putimage(0, MID_UP_BEGIN_Y, &mid_up, SRCPAINT);
#endif
    //�е���
    for (int i = 3; i < 6; i++) {
        ds[i].show();
    }
#if TEST
    putimage(0, MID_DOWN_BEGIN_Y, &mid_down_y, SRCAND);
    putimage(0, MID_DOWN_BEGIN_Y, &mid_down, SRCPAINT);
#endif
    //�����
    for (int i = 6; i < 9; i++) {
        ds[i].show();
    }
#if TEST
    putimage(0, BOTTOM_BEGIN_Y, &bottom_y, SRCAND);
    putimage(0, BOTTOM_BEGIN_Y, &bottom, SRCPAINT);
#endif

    EndBatchDraw();
}

//����
void ctolGame() {
    if (ds[m].isUp) {
        ds[m].moveUp();
        if (m >= 0 && m < 3) {
            if (ds[m].y <= LITTLE_MOUSE_BEGIN_Y - LITTLE_MOUSE_HEITH) {
                ds[m].isUp = false;
            }
        }
        if (m >= 3 && m < 6) {
            if (ds[m].y <= MID_MOUSE_BEGIN_Y - MID_MOUSE_HEITH) {
                ds[m].isUp = false;
            }
        }
        if (m >= 6 && m < 9) {
            if (ds[m].y <= BIG_MOUSE_BEGIN_Y - BIG_MOUSE_HEITH) {
                ds[m].isUp = false;
            }
        }
    }
    else {
        ds[m].moveDown();
        if (m >= 0 && m < 3) {
            if (ds[m].y >= LITTLE_MOUSE_BEGIN_Y) {
                ds[m].isUp = true;
                m = rand() % NUM;
            }
        }
        if (m >= 3 && m < 6) {
            if (ds[m].y >= MID_MOUSE_BEGIN_Y) {
                ds[m].isUp = true;
                m = rand() % NUM;
            }
        }
        if (m >= 6 && m < 9) {
            if (ds[m].y >= BIG_MOUSE_BEGIN_Y) {
                ds[m].isUp = true;
                m = rand() % NUM;
            }
        }
    }

}


//int main()
//{
//	//0.������
//	initgraph(WINDOW_WIDTH, WINDOW_HEITH,1);
//
//	//��ʾ����ͼƬ
//	/*IMAGE top,mid_up,mid_down,bottom;
//	IMAGE top_y, mid_up_y, mid_down_y, bottom_y;*/
//
//	loadimage(&top, "top.bmp");//����ͼƬ
//	loadimage(&mid_up, "mid_up.bmp");//����ͼƬ
//	loadimage(&mid_down, "mid_down.bmp");//����ͼƬ
//	loadimage(&bottom, "bottom.bmp");//����ͼƬ
//	loadimage(&top_y, "top_y.bmp");//����ͼƬ
//	loadimage(&mid_up_y, "mid_up_y.bmp");//����ͼƬ
//	loadimage(&mid_down_y, "mid_down_y.bmp");//����ͼƬ
//	loadimage(&bottom_y, "bottom_y.bmp");//����ͼƬ
//
//	putimage(0, 0, &top_y,SRCAND);//��ͼ
//	putimage(0, 0, &top,SRCPAINT);//��ͼ
//	putimage(0, MID_UP_BEGIN_Y, &mid_up_y, SRCAND);//��ͼ
//	putimage(0, MID_UP_BEGIN_Y, &mid_up, SRCPAINT);//��ͼ
//	putimage(0, MID_DOWN_BEGIN_Y, &mid_down_y, SRCAND);//��ͼ
//	putimage(0, MID_DOWN_BEGIN_Y, &mid_down, SRCPAINT);//��ͼ
//	putimage(0, BOTTOM_BEGIN_Y, &bottom_y, SRCAND);//��ͼ
//	putimage(0, BOTTOM_BEGIN_Y, &bottom, SRCPAINT);//��ͼ
//
//	while (1)
//	{
//
//	}
//
//	return 0;
//}