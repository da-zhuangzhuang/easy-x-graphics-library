#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<vector>
#include<queue>
class Solution {
public:
    int halveArray(vector<int>& nums) {
        priority_queue<double>heap;
        double sum;
        for (auto ch : nums)
        {
            sum += ch;
            heap.push(ch);
        }
        sum /= 2.0;
        int count = 0;
        while (sum > 0)
        {
            double t = heap.top() / 2;
            heap.pop();
            sum -= t;
            heap.push(t);
            count++;
        }
        return count;
    }
};
//int main()
//{
//	vector<int> v;
//
//
//}