#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int max0(double* arr, int len)
{
    int index = 0;
    for (int i = 0; i < len; i++)
    {
        if (arr[index] < arr[i])
        {
            index = i;
        }
    }
    return index;
}
int halveArray(int* nums, int numsSize) {
    double* arr = (double*)malloc(sizeof(double) * numsSize);
    for (int i = 0; i < numsSize; i++)
    {
        arr[i] = nums[i];
    }
    double sum = 0;
    for (int i = 0; i < numsSize; i++)
    {
        sum += arr[i];
    }
    sum /= 2;
    int max;
    int count = 0;
    while (sum > 0)
    {
        max = max0(arr, numsSize);
        arr[max] /= 2.0;
        sum -= arr[max];
        count++;
    }
    return count;

}
int main()
{
	
    int arr[] = { 32,98,23,14,67,40,26,9,96,96,91,76,4,40,42,2,31,13,16,37,62,2,27,25,100,94,14,3,48,56,64,59,33,10,74,47,73,72,89,69,15,79,22,18,53,62,20,9,76,64 };
    int count = halveArray(arr, sizeof(arr) / sizeof(arr[0]));
    printf("%d\n", count);
    return 0;
}