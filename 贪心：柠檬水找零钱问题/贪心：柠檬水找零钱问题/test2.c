#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdbool.h>
bool lemonadeChange(int* bills, int billsSize) {
    int five = 0, ten = 0;
    for (int i = 0; i < billsSize; i++)
    {
        if (bills[i] == 5)
            five++;
        else if (bills[i] == 10)
        {
            if (five < 1)return false;
            five--;
            ten++;
        }
        else {
            if (five && ten)//贪心攻略
            {
                five--;
                ten--;
            }
            else if (five >= 3)
                five -= 3;
            //  if(five>=3)//这个顺序不行
            // {
            //     five-=3;
            // }
            // else if(five&&ten)
            // {
            //     five--;
            //     ten--;
            // }
            else
                return false;
        }
    }
    return true;
}

//int main()
//{
//    int arr[] = { 5,10,5,10 };
//    if (lemonadeChange(arr, sizeof(arr) / sizeof(arr[0])))
//    {
//        printf("可以找零钱！\n");
//    }
//    else
//    {
//        
//            printf("不可以！\n");
//        
//    }
//
//    return 0;
//}