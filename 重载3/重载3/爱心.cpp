#include <iostream>
#include <cmath>
using namespace std;

int main() {
    const double PI = 3.14159265358979323846;
    const int WIDTH = 40;
    const int HEIGHT = 20;
    const double SCALE_X = 4.0 / WIDTH;
    const double SCALE_Y = 2.0 / HEIGHT;
    const double A = 1.5;
    const double B = 1.5;
    const double C = 1.0;
    const double D = 1.0;

    for (int y = HEIGHT - 1; y >= 0; y--) {
        for (int x = 0; x < WIDTH; x++) {
            double fx = (x - WIDTH / 2.0) * SCALE_X;
            double fy = (y - HEIGHT / 2.0) * SCALE_Y;
            double z = pow(fx * fx + fy * fy - 1.0, 3.0) - fx * fx * fy * fy * fy;
            if (z <= 0.0) {
                cout << "*";
            }
            else {
                cout << " ";
            }
        }
        cout << endl;
    }

    return 0;
}